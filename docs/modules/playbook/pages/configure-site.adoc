= Site Keys

The playbook keys configured under `site` manage the site's published identity and how it interacts with certain applications once it's published.

When building the site, some of the attributes defined in the playbook can be overridden on the xref:cli:options.adoc#generate-options[command line] or with xref:environment-variables.adoc[environment variables].

[#site-key]
== site key

Global generated site files, service accounts, and other common properties are defined under the `site` key in a playbook file.
These settings are applied to the entire site when it's generated.

[source,yaml]
----
site: # <1>
  title: Docs Site # <2>
  url: https://docs.example.org # <3>
  start_page: component-b::index.adoc # <4>
  robots: allow # <5>
  keys: # <6>
    google_analytics: XX-123456 # <7>
----
<1> Required `site` key
<2> Required `title` key
<3> Required `url` key
<4> Optional `start_page` key
<5> Optional `robots` key
<6> Optional `keys` key
<7> Example `google_analytics` key

The `site`, `title`, and `url` keys are required.
The other keys are optional; Antora will use their default values if they're not specified.
Many of these keys can also be configured from the xref:cli:index.adoc[Antora CLI] or with environment variables.

TIP: Alternatively, `title`, `start_page`, and `keys` can be xref:cli:options.adoc[assigned from the CLI].
The `url` key can be assigned from the xref:cli:options.adoc#site-url[CLI] or using an xref:environment-variables.adoc[environment variable].

[#site-reference]
== Available site keys

[cols="3,6,1"]
|===
|Site Keys |Description |Required

|xref:site-keys.adoc[keys]
|Account and API keys for reference by the UI templates or extensions.
Accepts a map of name-value pairs that specify account identifiers for service integrations such as Google Analytics.
|No

|xref:site-robots.adoc[robots]
|Specifies whether Antora generates a _robots.txt_ file.
Accepts the values `allow`, `disallow`, and a custom, multi-line string.
Ignored if the sibling `url` key is not set.
|No

|xref:site-start-page.adoc[start_page]
|Accepts a page ID that specifies the start page of a site.
|No

|xref:site-title.adoc[title]
|Specifies the title of a site.
|Yes

|xref:site-url.adoc[url]
|Specifies the base URL of a site.
|Yes
|===
